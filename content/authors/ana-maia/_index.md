---
# Display name
name: Ana Lucia Lima Marreiros Maia

# Username (this should match the folder name)
authors:
- ana-maia

# Is this the primary user of the site?
superuser: false

# Role/position
# ["Associate Professor", "Post-Doctoral Researcher", "PhD Student", "Master Student", "Undergrad Student"] 
role: PhD Student

# Organizations/Affiliations
organizations:
- name: University of São Paulo
  url: ""

#Value used to order authors list
weight: 20
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
user_groups:
- Collaborators
---

