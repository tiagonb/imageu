---
# Display name
name: Roberto Hirata Jr

# Username (this should match the folder name)
authors:
- hirata

# Is this the primary user of the site?
superuser: false

# Role/position
# ["Associate Professor", "Post-Doctoral Researcher", "PhD Student", "Master Student", "Undergrad Student"] 
role: Associate Professor

# Organizations/Affiliations
organizations:
- name: University of São Paulo
  url: "https://www.ime.usp.br/hirata/"

weight: 7

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
# ["Coordinator", "Associate Researchers", "Students", "Collaborators"]
user_groups:
- Associate Researchers
---

