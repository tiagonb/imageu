---
# Display name
name: Leonardo Blanger

# Username (this should match the folder name)
authors:
- leonardo-blanger

# Is this the primary user of the site?
superuser: false

# Role/position
# ["Associate Professor", "Post-Doctoral Researcher", "PhD Student", "Master Student", "Undergrad Student"] 
role: MSc Student

# Organizations/Affiliations
organizations:
- name: University of São Paulo
  url: ""

#Value used to order authors list
weight: 40 
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
# ["Coordinator", "Associate Researchers", "Student", "Collaborators"]
user_groups:
- Collaborators
---

