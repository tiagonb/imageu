---
# Display name
name: Harold Mouchère

# Username (this should match the folder name)
authors:
- mouchere

# Is this the primary user of the site?
superuser: false

# Role/position
# ["Associate Professor", "Post-Doctoral Researcher", "PhD Student", "Master Student", "Undergrad Student"] 
role: Professor

# Organizations/Affiliations
organizations:
- name: University of Nantes | France
  url: "http://pagesperso.ls2n.fr/~mouchere-h/Recherche.php?lg=en"

weight: 6

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
# ["Coordinator", "Associate Researchers", "Students", "Collaborators"]
user_groups:
- Associate Researchers
---

