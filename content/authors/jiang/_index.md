---
# Display name
name: Xiaoyi Jiang

# Username (this should match the folder name)
authors:
- jiang

# Is this the primary user of the site?
superuser: false

# Role/position
# ["Associate Professor", "Post-Doctoral Researcher", "PhD Student", "Master Student", "Undergrad Student"] 
role: Associate Professor

# Organizations/Affiliations
organizations:
- name: University of Münster | Germany
  url: "https://www.uni-muenster.de/PRIA/personen/jiang.shtml"

weight: 45

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
# ["Coordinator", "Associate Researchers", "Students", "Collaborators"]
user_groups:
- Collaborators
---

