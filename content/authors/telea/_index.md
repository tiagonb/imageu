---
# Display name
name: Alexandru C. Telea

# Username (this should match the folder name)
authors:
- telea

# Is this the primary user of the site?
superuser: false

# Role/position
# ["Associate Professor", "Post-Doctoral Researcher", "PhD Student", "Master Student", "Undergrad Student"] 
role: Associate Professor

# Organizations/Affiliations
organizations:
- name: Utrecht University | Netherlands
  url: "http://www.staff.science.uu.nl/~telea001/"

weight: 5

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
# ["Coordinator", "Associate Researchers", "Students", "Collaborators"]
user_groups:
- Associate Researchers
---

