---
# Display name
name: Sérgio Yuji Hayashi

# Username (this should match the folder name)
authors:
- sergio-hayashi

# Is this the primary user of the site?
superuser: false

# Role/position
# ["Associate Professor", "Post-Doctoral Researcher", "PhD Student", "Master Student", "Undergrad Student"] 
role: MSc Student

# Organizations/Affiliations
organizations:
- name: University of São Paulo
  url: ""

#Value used to order authors list
# pos-doc: 10, PhD: 20, MSc: 30, IC: 40, TF: 45 
weight: 60
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
# ["Coordinator", "Associate Researchers", "Student", "Collaborators"]
user_groups:
- Collaborators
---

