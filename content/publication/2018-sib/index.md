---
title: "A Machine Learning approach for Graph-based Page Segmentation"
date: 2018-01-01
publishDate: 2020-10-20T16:12:12.856314Z
authors: ["A. L. M. Maia", "F. D. Julca-Aguilar", "N. S. T. Hirata"]
publication_types: ["1"]
abstract: ""
featured: false
publication: "*31st Conference on Graphics, Patterns and Images (SIBGRAPI)*"
doi: "10.1109/SIBGRAPI.2018.00061"
---

