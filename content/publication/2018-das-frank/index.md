---
title: "Symbol detection in online handwritten graphics using Faster R-CNN"
date: 2018-01-01
publishDate: 2020-10-20T16:12:12.856564Z
authors: ["Frank D. Julca-Aguilar", "N. S. T. Hirata"]
publication_types: ["1"]
abstract: ""
featured: false
publication: "*13th IAPR International Workshop on Document Analysis Systems (DAS)*"
doi: "10.1109/DAS.2018.79"
---

