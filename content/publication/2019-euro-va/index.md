---
title: "Deep Learning Inverse Multidimensional Projections"
date: 2019-01-01
publishDate: 2020-10-20T16:12:12.856055Z
authors: ["Mateus Espadoto", "Francisco Caio Maia Rodrigues", "Nina S. T. Hirata", "Roberto Hirata Jr.", "Alexandru C. Telea"]
publication_types: ["1"]
abstract: ""
featured: false
publication: "*EuroVis Workshop on Visual Analytics (EuroVA)*"
doi: "10.2312/eurova.20191118"
---

