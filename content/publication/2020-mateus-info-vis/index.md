---
title: "Deep learning multidimensional projections"
date: 2020-01-01
publishDate: 2020-10-20T16:12:12.853827Z
authors: ["Mateus Espadoto", "N. S. T. Hirata", "Alexandru C. Telea"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Information Visualization*"
url_pdf: "https://doi.org/10.1177/1473871620909485"
doi: "10.1177/1473871620909485"
---

