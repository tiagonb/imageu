---
title: "Improving Neural Network-based Multidimensional Projections"
date: 2020-01-01
publishDate: 2020-10-20T16:12:12.854370Z
authors: ["Mateus Espadoto", "Nina S. T. Hirata", "Alexandre X. Falcão", "Alexandru C. Telea."]
publication_types: ["0"]
abstract: ""
featured: false
publication: "*Proceedings of the 15th International Joint Conference on Computer Vision, Imaging and Computer Graphics Theory and Applications - Volume 3: IVAPP*"
doi: "10.5220/0008877200290041"
---

