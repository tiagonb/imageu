---
title: "Towards a Quantitative Survey of Dimension Reduction Techniques"
date: To appear-01-01
publishDate: 2020-10-20T16:12:12.855184Z
authors: ["Mateus Espadoto", "Rafael Martins", "Andreas Kerren", "Nina Hirata", "Alexandru Telea"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*IEEE Transactions on Visualization and Computer Graphics*"
---

