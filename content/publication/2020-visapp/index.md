---
title: "Deep Learning for Astronomical Object Classification: A Case Study"
date: 2020-01-01
publishDate: 2020-10-20T16:12:12.854641Z
authors: ["Ana Martinazzo", "Mateus Espadoto", "Nina S. T. Hirata"]
publication_types: ["0"]
abstract: ""
featured: false
publication: "*Proceedings of the 15th International Joint Conference on Computer Vision, Imaging and Computer Graphics Theory and Applications - Volume 5: VISAPP*"
doi: "10.5220/0008939800870095"
---

