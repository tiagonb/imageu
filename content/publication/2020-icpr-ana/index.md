---
title: "Self-supervised Learning for Astronomical Image Classification"
date: 2020-01-01
publishDate: 2020-10-20T16:12:12.853121Z
authors: ["Ana Martinazzo", "Mateus Espadoto", "Nina S. T. Hirata"]
publication_types: ["1"]
abstract: ""
featured: false
publication: "*ICPR (to appear)*"
---

