---
title: "Skeletal Similarity based Structural Performance Evaluation for Document Binarization"
date: 2020 (To appear)-01-01
publishDate: 2020-10-20T16:12:12.853515Z
authors: ["Augusto Cesar Monteiro Silva", "Xiaoyi Jiang", "Nina Hirata"]
publication_types: ["1"]
abstract: ""
featured: false
publication: "*International Conference on Frontiers of Handwriting Recognition (ICFHR)*"
---

