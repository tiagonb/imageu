---
title: "A General Framework for the Recognition of Online Handwritten Graphics"
date: 2020-01-01
publishDate: 2020-10-20T16:12:12.854098Z
authors: ["Frank D. Julca-Aguilar", "Harold Mouchère", "Christian Viard-Gaudin", "Nina S. T. Hirata"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*International Journal on Document Analysis and Recognition (IJDAR)*"
url_pdf: ""
doi: "10.1007/s10032-019-00349-6"
---

