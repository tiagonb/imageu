---
title: "The Southern Photometric Local Universe Survey (S-PLUS): improved SEDs, morphologies and redshifts with 12 optical filters"
date: 2019-10-01
publishDate: 2020-10-20T16:12:12.855450Z
authors: ["C. Mendes de Oliveira", " \emphet.al."]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Monthly Notices of the Royal Astronomical Society*"
tags: ["galaxies: clusters: general", "galaxies: photometry", "(galaxies:) quasars: general", "stars: general", "surveys"]
doi: "10.1093/mnras/stz1985"
---

