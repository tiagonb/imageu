---
title: "Multiband image classification of astronomical objects"
date: 2019-01-01
publishDate: 2020-10-20T16:12:12.854902Z
authors: ["A. Martinazzo", "N. S. T. Hirata"]
publication_types: ["1"]
abstract: ""
featured: false
publication: "*Anais Estendidos da XXXII Conference on Graphics, Patterns and Images -- Workshop de Trabalhos em Andamento*"
---

