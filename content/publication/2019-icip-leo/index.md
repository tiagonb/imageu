---
title: "An Evaluation of Deep Learning Techniques for QR Code Detection"
date: 2019-01-01
publishDate: 2020-10-20T16:12:12.855801Z
authors: ["Leonardo Blanger", "N. S. T. Hirata"]
publication_types: ["1"]
abstract: ""
featured: false
publication: "*IEEE International Conference on Image Processing (ICIP)*"
---

