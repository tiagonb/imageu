---
##############
#Core metadata
##############

#the title of your page
title:

#an optional subtitle that will be displayed under the title
subtitle:

#one-sentence summary of the content on your page. The summary can be shown on the homepage and can also benefit your search engine ranking.
summary:

#the RFC 3339 date that the page was published. A future date will schedule the page to be published in the future. If you use the hugo new ... commands described on this page, the date will be filled automatically when you create a page. Also see lastmod and publishDate.
date:

#tagging your content helps users to discover similar content on your site. Tags can improve search relevancy and are displayed after the page content and also in the Tag Cloud widget. E.g. tags: ["Electronics", "Diodes"].
tags:

#by setting draft: true, only you will see your page when you preview your site locally on your computer
draft: 

profile: false  # Show author profile?

######### Example below ######################

title: "Template title"
subtitle: "Template subtitle"
summary: "Qual será o nome da linha de pesquisa"

draft: true

tags: 
 - structured data analysis
 - high condensed info extraction 
 - low latency document generatos

# Date is optional for research, if dont want show date, delete it
date: 2019-09-08T02:10:01-03:00 

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: Imagem de [**Alexas_Fotos**](https://pixabay.com/pt/users/Alexas_Fotos-686414/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=3094035) por [**Pixabay**](https://pixabay.com/pt/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=3094035)
  focal_point: "smart"
  preview_only: false

#url_code and url_dataset are used to show content in (data + code) section
url_code: ""
url_dataset: ""

url_pdf: ""
url_slides: ""
url_video: ""

#como escrever em markdown -> https://wowchemy.com/docs/writing-markdown-latex/
---

### Conteudo começa aqui


Coming up soon

### Comics image processing

Coming up soon
