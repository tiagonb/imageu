---
title: "Image Transformation"
summary: "orem ipsum iaculis blandit libero senectus erat ultricies, ultrices aptent conubia duis morbi habitant sociosqu ac, praesent bibendum fusce condimentum auctor tristique. venenatis platea faucibus iaculis conubia gravida purus luctus pulvinar cras, aliquam cras fames quisque odio nisi nibh ultricies justo curabitur, ligula aliquet est sodales tristique semper tempus purus. himenaeos integer venenatis facilisis ar"

draft: false

tags: 
 - structured data analysis
 - high condensed info extraction 
 - low latency document generatos

#date: 2019-09-08T02:10:01-03:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: Imagem de [**Alexas_Fotos**](https://pixabay.com/pt/users/Alexas_Fotos-686414/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=3094035) por [**Pixabay**](https://pixabay.com/pt/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=3094035)
  focal_point: "smart"
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

#url_code and url_dataset are used to show content in (data + code) section
url_code: ""
url_dataset: ""

url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

### Page segmentation


Coming up soon

### Comics image processing

Coming up soon
